using Infrastructure.Services;
using Infrastructure.StateMachine;
using Logic;

namespace Infrastructure
{
    public class Game
    {
        public GameStateMachine StateMachine;

        public Game(ICoroutineRunner coroutineRunner, LoadingCurtain loadingCurtain)
        {
            StateMachine = new GameStateMachine(new SceneLoader(coroutineRunner), ServiceLocator.Container, loadingCurtain);
        }
    }
}