namespace Logic
{
    public enum AnimatorState
    {
        Idle,
        Attack,
        Walking,
        Died,
        Unknown
    }
}