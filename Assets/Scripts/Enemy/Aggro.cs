using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace Enemy
{
    public class Aggro : MonoBehaviour
    {
        public TriggerObserver TriggerObserver;
        public Follow Follow;
        
        public float Cooldown;

        private Coroutine _aggroCoroutine;
        private bool _hasTarget;

        private void Start()
        {
            TriggerObserver.TriggerEnter += TriggerEnter;
            TriggerObserver.TriggerExit += TriggerExit;

            SwitchFollowOff();
        }

        private void TriggerEnter(Collider obj)
        {
            if (!_hasTarget)
            {
                _hasTarget = true;
                
                StopAggroCoroutine();
            }

            SwitchFollowOn(); 
        }

        private void StopAggroCoroutine()
        {
            if (_aggroCoroutine != null)
            {
                StopCoroutine(_aggroCoroutine);
                _aggroCoroutine = null;
            }
        }

        private void TriggerExit(Collider obj)
        {
            if (_hasTarget)
            {
                _hasTarget = false;
                
                _aggroCoroutine = StartCoroutine(SwitchFollowOffAfterCooldown());
            }
        }

        private void SwitchFollowOn()
        {
            Follow.enabled = true;
        }

        private void SwitchFollowOff()
        {
            Follow.enabled = false;
        }

        private IEnumerator SwitchFollowOffAfterCooldown()
        {
            yield return new WaitForSeconds(Cooldown);
            SwitchFollowOff();
        }
    }
}