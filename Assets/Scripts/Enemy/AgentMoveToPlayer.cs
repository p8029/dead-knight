using System;
using Infrastructure.Factory;
using Infrastructure.Services;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy
{
    public class AgentMoveToPlayer : Follow
    {
        private const float MinimalDistance = 1.0f;
        
        private NavMeshAgent _agent;
        private Transform _heroTransform;

        private IGameFactory _gameFactory;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _gameFactory = ServiceLocator.Container.Single<IGameFactory>();
        }

        private void Start()
        {
            if (_gameFactory.HeroGameObject != null)
            {
                InitializeHeroTransform();
            }
            else
            {
                _gameFactory.HeroCreated += HeroCreated;
            }
        }

        private void Update()
        {
            if (Initialized() && HeroNotReached())
            {
                _agent.destination = _heroTransform.position;
            }
        }

        private void InitializeHeroTransform()
        {
            _heroTransform = _gameFactory.HeroGameObject.transform;
        }

        private bool HeroNotReached()
        {
            return Vector3.Distance(_agent.transform.position, _heroTransform.position) >= MinimalDistance;
        }

        private void HeroCreated()
        {
            InitializeHeroTransform();
        }

        private bool Initialized()
        {
            return _heroTransform != null;
        }
    }
}