using System;
using Infrastructure.Factory;
using Infrastructure.Services;
using UnityEngine;

namespace Enemy
{
    public class RotateToHero : Follow
    {
        public float Speed;

        private Transform _heroTransform;
        private IGameFactory _gameFactory;
        private Vector3 _positionToLook;

        private void Awake()
        {
            _gameFactory = ServiceLocator.Container.Single<IGameFactory>();
        }

        private void Start()
        {
            if (HeroExist())
            {
                InitializeHeroTransform();
            }
            else
            {
                _gameFactory.HeroCreated += InitializeHeroTransform;
            }
        }

        private void Update()
        {
            if (Initialized())
            {
                RotateTowardsHero();
            }
        }

        private bool Initialized()
        {
            return _heroTransform != null;
        }

        private void InitializeHeroTransform()
        {
            _heroTransform = _gameFactory.HeroGameObject.transform;
        }

        private bool HeroExist()
        {
            return _heroTransform != null;
        }

        private void RotateTowardsHero()
        {
            UpdatePositionToLookAt();

            transform.rotation = SmoothedRotation(transform.rotation, _positionToLook);
        }

        private void UpdatePositionToLookAt()
        {
            Vector3 positionDiff = _heroTransform.position - transform.position;
            _positionToLook = new Vector3(positionDiff.x, transform.position.y, positionDiff.z);
        }

        private Quaternion SmoothedRotation(Quaternion transformRotation, Vector3 positionToLook)
        {
            return Quaternion.Lerp(transformRotation, TargetRotation(positionToLook), SpeedFactor());
        }

        private float SpeedFactor()
        {
            return Speed * Time.deltaTime;
        }

        private Quaternion TargetRotation(Vector3 positionToLook)
        {
            return Quaternion.LookRotation(positionToLook);
        }
    }
}